import argparse
import numpy as np
import tensorflow as tf

from functools import partial

from model.fcn import FCN
from util.model_loader import ModelLoader

from util import config, helper, image_data_generator


def main(conf):
    if hasattr(conf, 'model_path'):
        model = FCN(conf.input_shape, None)
    else:
        with ModelLoader(conf.inital_weights_path) as loader:
            model = FCN(conf.input_shape, loader, conf.num_classes, conf.checkpoint_frequency, conf.save_frequency,
                        conf.summary_frequency)

    tf_config = tf.ConfigProto()
    tf_config.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_1
    tf_config.gpu_options.per_process_gpu_memory_fraction = 0.9

    with tf.Session(config=tf_config) as sess:
        if hasattr(conf, 'model_path'):
            model.restore(sess, conf.model_path)
        elif conf.use_checkpoint:
            model.restore_checkpoint(sess, conf.checkpoint_dir)
        else:
            batch_gen = image_data_generator.generate_batch(conf.training_data_dir, conf.training_gt_dir,
                                                            image_data_generator.produce_kitti_segmap,
                                                            conf.batch_size, conf.input_shape)

            batch_func = image_data_generator.get_batches(batch_gen, conf.batch_size,
                                                          image_data_generator.build_sequential())

            loss = model.build_loss(conf.regularization_multiplier) if hasattr(conf, 'regularization_multiplier') \
                else model.build_loss()

            optimizer = model.build_optimizer(conf.learning_rate)

            model.train(sess, batch_func, conf.epochs, conf.keep_prob,
                        checkpoint_path=conf.checkpoint_dir, summary_path=conf.summary_dir,
                        model_path=conf.model_save_dir, loss=loss, optimizer=optimizer)

        print('Number of trainable weights: {}'.format(
            np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])))

        infer_func = partial(model.inference, sess)
        helper.save_inference_samples(infer_func, conf.runs_dir, conf.testing_dir, conf.input_shape)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        '-c', '--config_path',
        metavar='C',
        default='./config/fcn8s.yml',
        help='Configuration file as YAML or JSON')
    args = parser.parse_args()
    loaded = config.get_config(args.config_path)
    print('Using configuration: ' + str(loaded))

    main(loaded)
