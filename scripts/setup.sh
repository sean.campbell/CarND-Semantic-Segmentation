#!/bin/bash
# Provision spot instance; deep learning ubuntu AMI; p2 large ~$0.30ph
# Manual
# git clone https://gitlab.com/sean.campbell/CarND-Semantic-Segmentation.git project

declare -r src='https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/advanced_deep_learning/data_road.zip'
declare -r tgt='../data'

cd "$tgt"
wget "$src" -O data.zip
unzip data.zip
rm data.zip

# Place into appropriate folders
cd data_road/training
mkdir -p gt_road_2 gt_lane_2

cd gt_image_2
find . -name '*road*' -print0 | while IFS= read -r -d $'\0' file; do cp $file ../gt_road_2/${file/road_}; done;
find . -name '*lane*' -print0 | while IFS= read -r -d $'\0' file; do cp $file ../gt_lane_2/${file/lane_}; done;
cd ../../../..

# Install missing packages compared to older Udacity DL AMI
pip install --upgrade pip

# Install everything except the tensorflow libs
pip install -r requirements-aws.txt --user

# Required for imgaug
sudo apt-get install python3-tk
