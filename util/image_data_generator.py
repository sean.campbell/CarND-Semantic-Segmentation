import Augmentor
import imgaug as ia
from imgaug import augmenters as iaa, parameters as iap, SegmentationMapOnImage

import os
import numpy as np
import random

import scipy.misc


# deprecated
def build_pipeline(data_path, gt_path, batch_size):
    p = Augmentor.Pipeline(data_path)
    p.ground_truth(gt_path)

    p.rotate(probability=1.0, max_left_rotation=5, max_right_rotation=5)
    p.flip_left_right(probability=0.5)
    p.crop_random(probability=0.2, percentage_area=0.7)
    p.random_brightness(probability=1.0, min_factor=0.5, max_factor=1.5)
    p.random_contrast(probability=1.0, min_factor=0.7, max_factor=1.0)
    p.random_distortion(probability=0.2, grid_width=3, grid_height=3, magnitude=3)
    p.resize(probability=1.0, width=batch_size[0], height=batch_size[1], resample_filter='BICUBIC')

    return p
    # TODO Limitation of lib; can only do one-shot for masks
    # return p.keras_generator(batch_size=batch_size)


def produce_kitti_segmap(gt_image):
    background = np.asarray([255, 0, 0])
    return np.invert(np.all(np.asarray(gt_image) == background, axis=2))


def segmap_to_masks(segmap):
    vals = np.unique(segmap)
    init = np.expand_dims(segmap, -1)
    temp = np.concatenate([init == val for val in vals], axis=-1)
    return temp


def get_batches(gen_batches, batch_size, seq):
    def get_batch():
        loader = ia.BatchLoader(gen_batches, queue_size=batch_size * 2)
        augmenter = ia.BackgroundAugmenter(loader, seq)

        while True:
            batch = augmenter.get_batch()
            if batch is None:
                print('Batches exhausted!')
                break

            yield batch.images_aug, np.asarray([mask.arr > 0.1 for mask in batch.segmentation_maps_aug])

        loader.terminate()
        augmenter.terminate()

    return get_batch


# PIL issues; flipped dims?
# image = Image.open(os.path.join(data_path, image_file))
# gt = Image.open(os.path.join(gt_path, image_file))
#    image = image.resize(input_shape)
#    gt = image.resize(input_shape)
# temp = np.asarray(segmap_to_masks(gt_seg))
# scipy.misc.imsave('temp-rgb.png', image)
# scipy.misc.imsave('temp-back.png', np.array(segmap_to_masks(gt_seg))[:, :, 0].astype(np.uint8) * 255)
# scipy.misc.imsave('temp-road.png', np.array(segmap_to_masks(gt_seg))[:, :, 1].astype(np.uint8) * 255)
#
# for idx, segmask in enumerate(batch.segmentation_maps_aug):
# rar = segmask.arr
# image = batch.images_aug
# scipy.misc.imsave('./temp/{}-back.png'.format(idx), np.asarray(rar[:, :, 0]))
# scipy.misc.imsave('./temp/{}-image.png'.format(idx), np.asarray(image[idx, :, :, :]))
# scipy.misc.imsave('./temp/{}-front.png'.format(idx), np.asarray(rar[:, :, 1]))

def generate_batch(data_path, gt_path, produce_segmap, batch_size, input_shape=None):
    def get_batch():
        image_paths = os.listdir(data_path)
        random.shuffle(image_paths)

        for batch_i in range(0, len(image_paths), batch_size):
            images = []
            gts = []
            for image_file in image_paths[batch_i:batch_i + batch_size]:
                image = scipy.misc.imread(os.path.join(data_path, image_file))
                gt = scipy.misc.imresize(scipy.misc.imread(os.path.join(gt_path, image_file)), input_shape)

                if input_shape:
                    image = scipy.misc.imresize(image, input_shape)
                    gt = scipy.misc.imresize(gt, input_shape)

                gt_seg = produce_segmap(np.asarray(gt))

                images.append(np.asarray(image))
                gts.append(SegmentationMapOnImage(np.asarray(segmap_to_masks(gt_seg)), image.shape[:2]))

            ret = ia.Batch(images=np.asarray(images), segmentation_maps=gts)
            yield ret

    return get_batch


def build_sequential():
    return iaa.Sequential([
        iaa.Fliplr(0.5),
        # Geometric based
        iaa.Sometimes(0.0,
                      [
                          iaa.OneOf(
                              [
                                  iaa.Crop(percent=(0.0, 0.15)),
                                  # iaa.Affine(rotate=(-5, 5), shear=(-10, 10), order=1, mode=['constant', 'edge'],
                                  #           cval=(0, 255))
                              ]
                          )
                      ]
                      ),
        # General blurring or edge sharpening
        iaa.Sometimes(0.2,
                      [
                          iaa.OneOf(
                              [
                                  iaa.GaussianBlur(sigma=(0.0, 2.0)),
                                  iaa.MedianBlur(k=(3, 9)),
                                  iaa.MotionBlur(k=(3, 9), angle=(135, 225), direction=(0.0, 1.0)),
                                  iaa.Sharpen(alpha=(0.0, 1.0), lightness=(0.5, 1.5))
                              ]
                          )
                      ]
                      ),
        # Brightness
        iaa.Sometimes(0.2,
                      [
                          iaa.OneOf([
                              iaa.Multiply(mul=(0.5, 1.5)),
                              iaa.Add(value=(-20, 20)),
                              iaa.ContrastNormalization(alpha=(0.75, 1.25)),
                              iaa.Grayscale((0.0, 0.5)),
                              iaa.FrequencyNoiseAlpha(
                                  exponent=(-4, 0),
                                  first=iaa.Multiply((0.5, 1.5), per_channel=False),
                                  second=iaa.ContrastNormalization((0.75, 1.25))
                              )
                          ]
                          )
                      ]
                      ),
        # TODO Contrast; not released imgaug yet for Clahe + HistEq

        # Deformation; simulates rain to an extent but heavy distortion
        iaa.Sometimes(0.0,
                      [   # Simulates rain to an extent but heavy distortion
                          iaa.ElasticTransformation(alpha=iap.Normal(40, 5), sigma=iap.Normal(4.0, 0.2))
                      ]
                      ),
        # Noise addition
        iaa.Sometimes(0.05,
                      [
                          iaa.OneOf(
                              [
                                  iaa.SaltAndPepper(p=(0.02, 0.06)),
                                  iaa.AdditivePoissonNoise(lam=(16.0, 32.0), per_channel=True)
                              ]
                          )
                      ]
                      )
    ], random_order=True)
