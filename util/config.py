import json
import yaml
from bunch import Bunch


def __load_config(func, path):
    with open(path, 'r') as stream:
        lut = func(stream)
    return Bunch(lut), lut


def get_config(path):
    if path.endswith('.yml'):
        config, _ = __load_config(yaml.safe_load, path)
    elif path.endswith('.json'):
        config, _ = __load_config(json.load, path)
    else:
        raise ValueError('Unrecognised config file type!')

    return config
