import math
import numpy as np
import os
import requests
from tqdm import tqdm


class ModelLoader:

    def __init__(self, path, url='https://www.cs.toronto.edu/~frossard/vgg16/vgg16_weights.npz', cleanup=False):
        if path.lower().endswith('.npz'):
            self.path = path
            self.url = url
            self.cleanup = cleanup
        else:
            raise RuntimeError('Only supporting npz currently!')

    def __enter__(self):
        self.__maybe_download_model(self.path, self.url)
        self.weights = np.load(self.path, mmap_mode='r')

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.cleanup:
            os.remove(self.path)

    def load_weight(self, name):
        return self.weights[name]

    @staticmethod
    def __maybe_download_model(path, url):
        """
        Download and extract pretrained vgg model if it doesn't exist
        :param data_dir: Directory to download the model to
        """
        if os.path.isfile(path):
            print('Pre-trained weights located!')
        else:
            print('Downloading missing weights!')
            req = requests.get(url, stream=True)

            total_size = int(req.headers.get('content-length', 0))
            block_size, written = 1024, 0
            with open(path, 'wb') as file:
                for data in tqdm(req.iter_content(block_size),
                                 total=math.ceil(total_size // block_size), unit='KB',
                                 unit_scale=True):
                    written = written + len(data)
                    file.write(data)

            if total_size == 0 or written != total_size:
                raise RuntimeError('Unable to download the file correctly!')
