import tensorflow as tf
import numpy as np

import third.upsample as up


def get_conv2d_layer(previous, weights, bias, name):
    with tf.variable_scope(name) as scope:
        W = get_constant(weights, "weights")
        b = get_constant(bias, "bias")

        layer = tf.nn.conv2d(previous, W, [1, 1, 1, 1], padding='SAME')
        layer = tf.nn.relu(tf.nn.bias_add(layer, b), name=scope.name)

    return layer


def get_conv2d_transpose_layer(previous, factor, shape, name):
    with tf.variable_scope(name) as scope:
        W = get_bilinear_weights(factor, shape[-1])
        b = get_constant(np.repeat(0.0, shape[-1]), "bias")

        layer = tf.nn.conv2d_transpose(previous, W, shape,
                                       strides=[1, factor, factor, 1],
                                       padding='SAME')
        layer = tf.nn.bias_add(layer, b, name=scope.name)

    return layer


def get_max_pool_2x2(previous, name):
    return tf.nn.max_pool(previous, ksize=[1, 2, 2, 1],
                          strides=[1, 2, 2, 1], padding="SAME", name=name)


def get_skip_connect(previous, shape, name, stddev=0.001):
    # From paper:
    # Thus, our skips are implemented by first scoring each layer to be fused by
    # 1 × 1 convolution
    # Couldn't see init mentioned for these scoring layers or if bias term learned
    with tf.variable_scope(name) as scope:
        W = tf.get_variable(initializer=tf.truncated_normal(shape, stddev=stddev), name="weights")
        b = get_constant(np.repeat(0.0, shape[-1]), "bias")

        layer = tf.nn.conv2d(previous, W, [1, 1, 1, 1], padding='SAME')
        layer = tf.nn.bias_add(layer, b, name=scope.name)

    return layer


def get_bilinear_weights(factor, num_classes):
    init = up.bilinear_upsample_weights(factor, num_classes)
    return get_constant(init, "bilinear_weights")


def get_constant(value, name):
    return tf.get_variable(initializer=tf.constant_initializer(dtype=tf.float32,value=value),
                           shape=value.shape, name=name)

def get_summary_op(var):
    mean = tf.reduce_mean(var)
    tf.summary.scalar('mean', mean)
    tf.summary.scalar('variance', tf.reduce_mean(tf.square(var - mean)))
    tf.summary.scalar('max', tf.reduce_max(var))
    tf.summary.scalar('min', tf.reduce_min(var))
    tf.summary.histogram('hist', var)
