# Semantic Segmentation

### Contents
1. [Overview](#overview)
2. [Prerequisites](#prerequisites)
3. [Augmentation](#augmentation)
4. [Network](#network)
5. [Implementation](#implementation)
6. [Samples](#samples)

## Overview
This is a TensorFlow implementation of the FCN8s model described in the paper [Fully Convolutional Networks for Semantic Segmentation](https://people.eecs.berkeley.edu/~jonlong/long_shelhamer_fcn.pdf).

In-line with the paper, it opts for an all-at-once training approach from a VGG 16-layer network, outlined in [Very Deep Convolutional Networks for Large-scale Image Recognition](https://arxiv.org/pdf/1409.1556.pdf). 

The [Kitti Road dataset](http://www.cvlibs.net/datasets/kitti/eval_road.php) has been explored and the inference portion on top of the trained network is capable of binary image classification, producing reasonable road and non-road masks with only rare false positive regions.  

The dataset is small by today's standards but the samples are widely varied meaning the network is capable of producing accurate results even without the addition of image augmentation.  Augmentation had been employed, however, in order to improve network robustness with respect to changing image conditions, such as brightness, noise and blurring. 

![Network Sample](output/b4-r1e-4-e40.gif)

## Prerequisites
This project has been implemented using [Python 3](https://www.python.org/), with the frozen pip requirements present in the repo.

A quick [script](scripts/fetch.sh) was written to speed-up the process when running on AWS, fetching the dataset and altering the structure of the data folder.  The latter essentially allows an exact one-to-one mapping with respect to image name between the RGB image and the training mask through moving ground truths into road or lane folders respectively.

## Augmentation
Through performing a transformation or series of transformation on the training data, network robustness can potentially be improved through avoiding data overfitting.

[Augmentor](https://github.com/mdbloice/Augmentor) was the first library trialled and had quite a few excellent features.  One of the weaknesses of performing random padding/rotation/shearing is that information at the edges must be specified, with commons solutions being to fill with constant values or replicate the closest pixel.  This library instead opted to crop away these sections and produce effectively a scaled version of the image simultaneously.  

Unfortunately, it did not support simultaneous transformation of both the image and ground truth at runtime (it is only capable of creating one-hot encoded labels through a generator).  The maintainer appeared to have a preference for writing the augmentation results to disk and using the simply using multiple folders at runtime whereas I prefer to generate transformed versions at runtime.

The excellent [imgaug](https://imgaug.readthedocs.io/en/latest/index.html) library was instead employed and has different advantages, namely offering a wide variety of transformations and the option to generate augmented batches in background threads.  
The pipeline used in this work consisted of the following transforms:

- Geometric: Flipping and Cropping

- Blurring/Sharpening:  One of Gaussian / Median / Motion blurring or alternatively image sharpening

- Brightness: One of Multiplication / Addition / Contrast Normalization / Grayscale / Overlay
 
- Deformation: Elastic Transformation

- Noise: Either Salt & Pepper or Additive Poisson Noise

These operations were performed with random probabilities in random orders and transformations from the same groupings indicated above would not be performed in conjunction with one another.
The parameters of these operations were predominantly set empirically.

Of interest is the elastic transformation which was an effort to simulate the effect of rainfall on the camera lens.

A sample of some of the operations is illustrated below applied to a single image and the respective ground truth is illustrated as two transformed masks.

![Augmentation Sample](output/example_segmaps.jpg)

## Network
The network architecture adheres, to the best of my understanding, to the paper outlined.  The main graph is illustrated below, generated through Tensorboard.
 
![Network Architecture](output/network_architecture.png)

A large number of summaries are produced for each network layer, enabling one to easily see the evolution of the network over time with regard to characteristics such as weight and bias histograms, means and standard deviations.

The below histograms illustrated the effect of the regularization weight on the shape of the final upsampling layer, with the former having a weighting five times greater than the latter.

![Tensorboard Histogram sample higher regularization weight](output/b4-r5e-4-e30-hist-sample.png)

![Tensorboard Histogram sample lower regularization weight](output/b4-r1e-4-e40-hist-sample.png)

The loss across time is additionally illustrated below, for the cases above.

![Tensorboard Loss sample higher regularization weight](output/b4-r5e-4-e30-loss.png)

![Tensorboard Loss sample lower regularization weight](output/b4-r1e-4-e40-loss.png)

Both the output directories are available for comparison, though the differences are relatively small for the training dataset.

The effect of batch size proved interesting with respect to rate of convergence but I settled on a batch size of four as eight seemed to converge much slower with respect to epoch, though computation time was naturally faster due to higher parallelism.
Further, I was training on a Tesla K80 and through using the smaller batch size, I was able to train two instances of the FCN8s network in parallel (the network had a total of 134,291,788 trainable weights).

## Implementation
The model was trained from the base VGG network weights rather than the version provided by the original repository, mainly out of the desire to replicate the paper exactly.

The code produced is relatively involved, with the main files of interest likely [FCN](model/fcn.py), [Image Augmentation](util/image_data_generator.py) and the [Runner](runner.py).

The majority of the code is suitably generic and the network architecture can be easily swapped out.  

One can similarly swap in their own custom loss function / optimizer / summary operation for training purposes.  The loss function by default is the sum of the cross entropy loss and regularization loss across all weight layers, with the weight associated with the latter loaded.  An AdamOptimizer is used by default, due to known popularity and effectiveness in literature.  The summary operation is predominantly max/min/mean/stddev of layers, their respective histograms and distributions.

Most configuration was externalised and loaded from a YAML file, making it easier to quickly re-train the network according to different hyper-parameters.  

All paths were similarly externalised, allowing multiple networks to be trained simultaneously provided there is a big enough GPU and the upper threshold on GPU usage is set per process.

## Samples
The animation gif illustrated above is a slow slideshow of all of the test data samples.  The images can be found in [samples](output/samples).

## Notes
There is a great blog on setting up tensorflow-gpu locally [here](https://tinyurl.com/ydzb57rg) using CUDA-10.  Ironically, the GTX 1050Ti (4Gb) in my notebook did not have the capacity to train the FCN8s network in its entirety! 
