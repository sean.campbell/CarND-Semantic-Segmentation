import tensorflow as tf
import numpy as np
import os
import re
import time
import warnings

import util.model_builder as mb


class FCN:

    def __init__(self, input_shape, model_loader, num_classes=2, checkpoint_frequency=2, save_frequency=10,
                 summary_frequency=20):
        self.input_shape = input_shape
        self.loader = model_loader

        self.num_classes = num_classes
        self.checkpoint_frequency = checkpoint_frequency
        self.save_frequency = save_frequency
        self.summary_frequency = summary_frequency

        if model_loader:
            self.data, self.labels, self.dropout = self.build_placeholders()
            self.network = self.build_network()
            self.softmax, self.argmax = self.build_predictor(self.network)

    def build_placeholders(self):
        with tf.name_scope('input'):
            self.data = tf.placeholder(tf.float32, [None, self.input_shape[0], self.input_shape[1], 3], name='data')
            self.labels = tf.placeholder(tf.float32, [None, self.input_shape[0], self.input_shape[1], self.num_classes],
                                         name='labels')

            self.dropout = tf.placeholder(tf.float32, name='dropout')
            # self.global_step = tf.Variable(0, dtype=tf.uint16, trainable=False, name="global_step")

        return self.data, self.labels, self.dropout

    def build_network(self):
        pre = self.__preprocess(self.data)

        # Build VGG convolution layers
        conv1 = self.__repeat_conv2d(pre, 2, 'conv1')
        pool1 = mb.get_max_pool_2x2(conv1, 'pool1')

        conv2 = self.__repeat_conv2d(pool1, 2, 'conv2')
        pool2 = mb.get_max_pool_2x2(conv2, 'pool2')

        conv3 = self.__repeat_conv2d(pool2, 3, 'conv3')
        pool3 = mb.get_max_pool_2x2(conv3, 'pool3')

        conv4 = self.__repeat_conv2d(pool3, 3, 'conv4')
        pool4 = mb.get_max_pool_2x2(conv4, 'pool4')

        conv5 = self.__repeat_conv2d(pool4, 3, 'conv5')
        pool5 = mb.get_max_pool_2x2(conv5, 'pool5')

        # Build VGG FC layers reshaped with 1x1 convolutions
        fc6 = self.__load_and_reshape_fully_connected(pool5, [7, 7, 512, 4096], 'fc6', self.dropout)
        fc7 = self.__load_and_reshape_fully_connected(fc6, [1, 1, 4096, 4096], 'fc7', self.dropout)
        fc8 = self.__last(fc7, [1, 1, 4096, self.num_classes], 'fc8')

        # Build scoring and upsampling layers
        s = tf.shape(self.data)
        s = [s[0], s[1], s[2], self.num_classes]

        up1 = mb.get_conv2d_transpose_layer(fc8, 32, s, 'up1')

        skip1 = mb.get_skip_connect(pool4, [1, 1, 512, self.num_classes], 'skip1')
        up2 = mb.get_conv2d_transpose_layer(skip1, 16, s, 'up2')

        skip2 = mb.get_skip_connect(pool3, [1, 1, 256, self.num_classes], 'skip2')
        up3 = mb.get_conv2d_transpose_layer(skip2, 8, s, 'up3')

        # Perform scaling and summation
        self.network = tf.add_n([up3, tf.multiply(up2, 2), tf.multiply(up1, 4)], name='output')

        return self.network

    def build_loss(self, regularization_multiplier=None):
        with tf.variable_scope('loss'):
            reshaped_logits = tf.reshape(self.network, (-1, self.num_classes))
            reshaped_labels = tf.reshape(self.labels, (-1, self.num_classes))

            # Approximate loss
            loss = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits_v2(logits=reshaped_logits, labels=reshaped_labels),
                name='cross_entropy_loss')

            # Add regularization loss
            if regularization_multiplier:
                reg_loss = tf.add_n([tf.nn.l2_loss(v) for v in tf.trainable_variables() if 'bias' not in v.name],
                                    name='reg_loss') * regularization_multiplier

                loss = tf.add(loss, reg_loss, name='loss')

        return loss

    @staticmethod
    def build_optimizer(learning_rate=1e-5):
        return tf.train.AdamOptimizer(learning_rate=learning_rate, name='adam_optimizer')

    def build_summaries(self, loss, gvs):
        # Text and charts
        with tf.name_scope('performance'):
            tf.summary.scalar('loss', loss)

        for grad, var in gvs:
            lname = var.name.replace('/', '-').replace(':', '-')
            with tf.name_scope('layer-stats-' + lname):
                mb.get_summary_op(grad)

        # Images
        if False:
            with tf.name_scope('tensorboard-images'):
                tf.summary.image('input', self.data, 1)

                mask = tf.greater(tf.reshape(self.network[:, :, :, 1], self.input_shape),
                                  tf.multiply(tf.ones(self.input_shape), 0.5))
                mask_s = tf.stack([mask, mask, mask], 2)
                out = tf.multiply(self.data, tf.to_float(mask_s))

                tf.summary.image('output', out, 1)

        return tf.summary.merge_all()

    def build_predictor(self, network):
        with tf.name_scope('prediction'):
            softmax_predictor = tf.nn.softmax(network, name='softmax')
            argmax_predictor = tf.argmax(softmax_predictor, axis=-1, name='argmax')

        return softmax_predictor, argmax_predictor

    def train(self, sess, train_gen, epochs=20, keep_prob=0.8,
              checkpoint_path='state/checkpoints', summary_path='state/summary', model_path='state/models',
              loss=None, optimizer=None, summary_op=None):
        if loss is None:
            loss = self.build_loss()

        if optimizer is None:
            optimizer = self.build_optimizer()

        if summary_op is None:
            summary_op = self.build_summaries(loss, optimizer.compute_gradients(loss))

        if not tf.test.gpu_device_name():
            warnings.warn('No GPU found located!  Training speed will be higher!')
        else:
            print('Located GPU: {}'.format(tf.test.gpu_device_name()))

        self.__train(sess, train_gen, epochs, keep_prob, checkpoint_path, summary_path,
                     model_path, loss, optimizer.minimize(loss), summary_op)

    def __train(self, sess, train_gen, epochs, keep_prob, checkpoint_path, summary_path,
                model_path, loss, training_op, summary_op):
        # Init
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        starting_epoch = self.restore_checkpoint(sess, checkpoint_path)
        summary_writer = tf.summary.FileWriter(summary_path, sess.graph)

        losses = [100000000]
        save_map = {'pending_checkpoint_save': False, 'pending_model_save': False}

        for epoch in range(starting_epoch, epochs):
            start = time.time()
            batch_losses = []
            for batch_num, (images, labels) in enumerate(train_gen()):
                if batch_num % self.summary_frequency == 0:
                    _, batch_loss, summary = sess.run([training_op, loss, summary_op],
                                                      feed_dict={self.data: images,
                                                                 self.labels: labels,
                                                                 self.dropout: keep_prob})
                    summary_writer.add_summary(summary, global_step=epoch)
                else:
                    _, batch_loss = sess.run([training_op, loss],
                                             feed_dict={self.data: images,
                                                        self.labels: labels,
                                                        self.dropout: keep_prob})
                #print('Completed batch')
                batch_losses.append(batch_loss)
            end = time.time()

            epoch_loss = sum(batch_losses) / len(batch_losses)
            self.__determine_save(epoch, epoch_loss, losses, save_map, sess, checkpoint_path, model_path)

            losses.append(epoch_loss)
            print(f"Epoch: {epoch:5} Average Loss: {epoch_loss:10} Time elapsed: {end - start: 10}")

    def inference(self, sess, data):
        reshaped_net = tf.reshape(self.network, (-1, self.num_classes))
        return sess.run([tf.nn.softmax(reshaped_net)], feed_dict={self.data: [data], self.dropout: 1.0})

    def __determine_save(self, epoch, current_loss, losses, save_map, sess, checkpoint_path, model_path):
        # Only save if improvement
        saveable = current_loss < min(losses)

        if (epoch + 1) % self.checkpoint_frequency == 0 or save_map['pending_checkpoint_save']:
            if saveable:
                print('Saving a new checkpoint for epoch {}.'.format(epoch))
                self.__save_checkpoint(sess, os.path.join(checkpoint_path, 'fcn-checkpoint-{}'.format(epoch)))
                save_map['pending_checkpoint_save'] = False
            else:
                save_map['pending_checkpoint_save'] = True

        if (epoch + 1) % self.save_frequency == 0 or save_map['pending_model_save']:
            if saveable:
                print('Saving a new model version for epoch {}'.format(epoch))
                self.save(sess, os.path.join(model_path, str(epoch)))
                save_map['pending_model_save'] = False
            else:
                save_map['pending_model_save'] = True

    @staticmethod
    def save(sess, path):
        saver = tf.saved_model.builder.SavedModelBuilder(path)
        saver.add_meta_graph_and_variables(sess=sess, tags=['default'])
        saver.save()

    # Assume graph will be cleared appropriately
    def restore(self, sess, path):
        tf.saved_model.loader.load(sess, tags=['default'], export_dir=path)
        graph = tf.get_default_graph()

        # Grab placeholders
        with tf.name_scope('input') as scope:
            self.data = graph.get_tensor_by_name(scope + 'data:0')
            self.labels = graph.get_tensor_by_name(scope + 'labels:0')
            self.dropout = graph.get_tensor_by_name(scope + 'dropout:0')

        # Grab network output
        self.network = graph.get_tensor_by_name('output:0')

        # Get predictors
        with tf.name_scope('prediction') as scope:
            self.softmax = graph.get_tensor_by_name(scope + 'softmax:0')
            self.argmax = graph.get_tensor_by_name(scope + 'argmax:0')

        return self.data, self.labels, self.dropout, self.network, self.softmax, self.argmax

    @staticmethod
    def __save_checkpoint(sess, path):
        tf.train.Saver(max_to_keep=3).save(sess, path)

    @staticmethod
    def restore_checkpoint(sess, path):
        ret = 0
        checkpoint = tf.train.get_checkpoint_state(path)
        if checkpoint:
            latest = tf.train.latest_checkpoint(path)
            print('Restoring from checkpoint: ' + latest)
            saver = tf.train.Saver()
            saver.restore(sess, latest)
            # Parse out the next epoch number
            ret = int(re.search(r'\d+', latest).group(0)) + 1

        return ret

    @staticmethod
    def __preprocess(previous):
        mean = tf.constant([123.68, 116.779, 103.939],  # From VGG, RGB
                           dtype=tf.float32, shape=[1, 1, 1, 3], name='mean')
        return previous - mean

    def __repeat_conv2d(self, previous, num, name_prefix):
        layer = previous
        for idx in range(1, num + 1):
            name = '{}_{}'.format(name_prefix, idx)
            weights = self.loader.load_weight(name + '_W')
            bias = self.loader.load_weight(name + '_b')

            layer = mb.get_conv2d_layer(layer, weights, bias, name)

        return layer

    def __last(self, previous, shape, name, stddev=0.0001):
        with tf.variable_scope(name) as scope:
            W = tf.get_variable(initializer=tf.truncated_normal(shape, stddev=stddev),
                                name='weights')
            b = mb.get_constant(np.repeat(0.0, shape[-1]), "bias")

            layer = tf.nn.conv2d(previous, W, [1, 1, 1, 1], padding='SAME')
            layer = tf.nn.bias_add(layer, b, name=name)

        return layer

    def __load_and_reshape_fully_connected(self, previous, shape, name, dropout):
        with tf.variable_scope(name) as scope:
            W = mb.get_constant(self.__reshape_fully_connected(name + '_W', shape), 'weights')
            b = mb.get_constant(self.loader.load_weight(name + '_b'), 'bias')

            layer = tf.nn.conv2d(previous, W, [1, 1, 1, 1], padding='SAME')
            layer = tf.nn.relu(tf.nn.bias_add(layer, b))
            layer = tf.nn.dropout(layer, dropout, name=scope.name)

        return layer

    def __reshape_fully_connected(self, name, shape):
        return self.loader.load_weight(name).reshape(shape)
