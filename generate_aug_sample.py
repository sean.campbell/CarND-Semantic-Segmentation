import imgaug as ia
import imageio
import numpy as np
from PIL import Image
import util.image_data_generator as h
from util.image_data_generator import build_sequential

ia.seed(1)

# Load an example image (uint8, 128x128x3).
image = np.asarray(Image.open('./data/data_road/training/image_2/um_000001.png'))
gt = np.asarray(Image.open('./data/data_road/training/gt_road_2/um_000001.png'))

# Create an example segmentation map (int32, 128x128).
# Here, we just randomly place some squares on the image.
# Class 0 is the background class.

segmap = np.invert(np.all(np.asarray(gt) == np.asarray([255, 0, 0]), axis=2))
masks = h.segmap_to_masks(segmap)

segmapOnImage = ia.SegmentationMapOnImage(masks, shape=image.shape, nb_classes=2)

# Define our augmentation pipeline.
seq = build_sequential()

# Augment images and heatmaps.
images_aug = []
segmaps_aug = []
for _ in range(5):
    seq_det = seq.to_deterministic()
    images_aug.append(seq_det.augment_image(image))
    segmaps_aug.append(seq_det.augment_segmentation_maps([segmapOnImage])[0])

# We want to generate an image of original input images and heatmaps before/after augmentation.
# It is supposed to have five columns: (1) original image, (2) augmented image,
# (3) augmented heatmap on top of augmented image, (4) augmented heatmap on its own in jet
# color map, (5) augmented heatmap on its own in intensity colormap,
# We now generate the cells of these columns.
#
# Note that we add a [0] after each heatmap draw command. That's because the heatmaps object
# can contain many sub-heatmaps and hence we draw command returns a list of drawn sub-heatmaps.
# We only used one sub-heatmap, so our lists always have one entry.
cells = []
for image_aug, segmap_aug in zip(images_aug, segmaps_aug):
    cells.append(image)  # column 1
    cells.append(segmapOnImage.draw_on_image(image))  # column 2
    cells.append(image_aug)  # column 3

    back = (np.asarray(segmap_aug.arr[:, :, 0]) > 0.1).astype(np.uint8) * 255
    fore = (np.asarray(segmap_aug.arr[:, :, 1]) > 0.1).astype(np.uint8) * 255

    cells.append(np.stack([back, back, back], -1))
    cells.append(np.stack([fore, fore, fore], -1))

    # Use below instead for overlay
    #cells.append(segmap_aug.draw_on_image(image_aug))  # column 4
    #cells.append(segmap_aug.draw(size=image_aug.shape[:2]))  # column 5

# Convert cells to grid image and save.
grid_image = ia.draw_grid(cells, cols=5)
imageio.imwrite("./output/example_segmaps.jpg", grid_image)
